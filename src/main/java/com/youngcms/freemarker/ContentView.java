package com.youngcms.freemarker;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.youngcms.bean.Content;
import com.youngcms.bean.ContentImage;
import com.youngcms.bean.ContentOption;
import com.youngcms.bean.ContentText;
import com.youngcms.bean.ContentVideo;
import com.youngcms.bean.Model;
import com.youngcms.freemarker.utils.Freemarker;
import com.youngcms.service.ChannelService;
import com.youngcms.service.ContentImageService;
import com.youngcms.service.ContentOptionService;
import com.youngcms.service.ContentService;
import com.youngcms.service.ContentTextService;
import com.youngcms.service.ContentVideoService;
import com.youngcms.service.ModelService;
import com.youngcms.utils.DictUtil;
import com.youngcms.vo.ContentDetail;
import com.youngcms.vo.QueryResult;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.WrappingTemplateModel;
/**
 * 内容详情
 * @author fumiao-pc
 *
 */
@Repository
public class ContentView implements TemplateDirectiveModel {
	
	@Autowired
	private ChannelService channelService;
	@Autowired
	private ContentService contentService;
	@Autowired
	private ModelService modelService;
	@Autowired
	private ContentTextService contentTextService;
	@Autowired
	private ContentImageService contentImageService;
	@Autowired
	private ContentOptionService contentOptionService;
	@Autowired
	private ContentVideoService contentVideoService;

	@Override
	public void execute(Environment env, Map map, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		try {
			Integer contentId = Freemarker.getInteger(map, "contentId");
			Content content = contentService.selectByPrimaryKey(contentId);
			Model model = modelService.selectByPrimaryKey(content.getModelId());
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("contentId", content.getId());
			ContentDetail contentDetail = new ContentDetail();
			BeanUtils.copyProperties(contentDetail, content);
			if (model.getHasContent() == DictUtil.getIdByNameAndEnName("hasContent", "是")) {
				QueryResult<ContentText> queryResult = contentTextService.list(params);
				if (queryResult.getCount() > 0) {
					ContentText contentText = queryResult.getQueryResult().get(0);
					contentDetail.setContentText(contentText);
				}
			}
			if (model.getHasGroupImages() == DictUtil.getIdByNameAndEnName("hasGroupImages", "是")) {
				QueryResult<ContentImage> queryResult = contentImageService.list(params);
				contentDetail.setContentImages(queryResult.getQueryResult());
			}
			if (model.getHasVedio() == DictUtil.getIdByNameAndEnName("hasVedio", "是")) {
				QueryResult<ContentVideo> queryResult = contentVideoService.list(params);
				if (queryResult.getCount() > 0) {
					ContentVideo contentVideo = queryResult.getQueryResult().get(0);
					contentDetail.setContentVideo(contentVideo);
				}
			}
			if (model.getHasOptions() == DictUtil.getIdByNameAndEnName("hasOptions", "是")) {
				QueryResult<ContentOption> queryResult = contentOptionService.list(params);
				contentDetail.setContentOptions(queryResult.getQueryResult());
			}
			loopVars[0] = WrappingTemplateModel.getDefaultObjectWrapper().wrap(contentDetail);
			body.render(env.getOut());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

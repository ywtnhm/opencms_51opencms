package com.youngcms.dao;

import com.youngcms.bean.FlowFace;
import com.youngcms.dao.base.MapperSupport;

public interface FlowFaceMapper extends MapperSupport<FlowFace> {
}
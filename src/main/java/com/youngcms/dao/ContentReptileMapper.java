package com.youngcms.dao;

import com.youngcms.bean.ContentReptile;
import com.youngcms.dao.base.MapperSupport;

public interface ContentReptileMapper extends MapperSupport<ContentReptile> {
}
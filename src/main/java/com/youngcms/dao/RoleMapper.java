package com.youngcms.dao;

import com.youngcms.bean.Role;
import com.youngcms.dao.base.MapperSupport;

public interface RoleMapper extends MapperSupport<Role> {
}
package com.youngcms.dao;

import com.youngcms.bean.Advert;
import com.youngcms.dao.base.MapperSupport;

public interface AdvertMapper extends MapperSupport<Advert> {
    
}
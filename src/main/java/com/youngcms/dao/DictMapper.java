package com.youngcms.dao;

import com.youngcms.bean.Dict;
import com.youngcms.dao.base.MapperSupport;

public interface DictMapper extends MapperSupport<Dict> {
}
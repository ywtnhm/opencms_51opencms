package com.youngcms.dao;

import com.youngcms.bean.ContentImage;
import com.youngcms.dao.base.MapperSupport;

public interface ContentImageMapper extends MapperSupport<ContentImage> {
}
package com.youngcms.dao;

import com.youngcms.bean.User;
import com.youngcms.dao.base.MapperSupport;

public interface UserMapper extends MapperSupport<User> {
}
package com.youngcms.dao;

import com.youngcms.bean.FlowGroup;
import com.youngcms.dao.base.MapperSupport;

public interface FlowGroupMapper extends MapperSupport<FlowGroup> {
}
package com.youngcms.dao;

import com.youngcms.bean.AdvertPosition;
import com.youngcms.dao.base.MapperSupport;

public interface AdvertPositionMapper extends MapperSupport<AdvertPosition> {

}
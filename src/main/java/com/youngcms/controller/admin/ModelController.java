package com.youngcms.controller.admin;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.youngcms.bean.Model;
import com.youngcms.controller.base.AdminBaseController;
import com.youngcms.service.ModelService;
import com.youngcms.utils.DateUtil;
import com.youngcms.utils.DictUtil;
import com.youngcms.utils.ListTypeParameter;
import com.youngcms.vo.ActionResult;
import com.youngcms.vo.QueryResult;
@Controller
@RequestMapping("/admin/model/")
@SessionAttributes("pageForm")
public class ModelController extends AdminBaseController{

     @Autowired
     private ModelService modelService;

     @RequestMapping("list")
     public ModelAndView list(Model model,HttpServletRequest request) throws Exception{
        QueryResult<Model> queryResult=modelService.list(model);
        model.setPageDate(queryResult.getQueryResult());
        model.setTotalCount(queryResult.getCount());
        ModelAndView mnv = new ModelAndView();
        mnv.addObject(DEFAULT_PAGE_VIEW,model);
        mnv.setViewName("view/admin/model/list");
        return mnv;
     }

     @RequestMapping("delete")
     @ResponseBody
     public ActionResult delete(Model model,HttpServletRequest request) throws Exception{
        ActionResult result=new ActionResult();
       try {
	     modelService.deleteByPrimaryKey(model.getId());
	     result.setSuccess(true);
	     result.setMessage(RESULE_SUCCESS_DELETE);
		 } catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR_DELETE);
		}
		return result;
	 }

	 @RequestMapping("showForm")
	 public ModelAndView addForm(Model model,HttpServletRequest request) throws Exception{
		ModelAndView mnv = new ModelAndView();
		if(model.getId()!=null){
			model=modelService.selectByPrimaryKey(model.getId());
		}else{
			model.setHasContent(DictUtil.getIdByNameAndEnName("hasContent", "是"));
			model.setHasGroupImages(DictUtil.getIdByNameAndEnName("hasGroupImages", "否"));
			model.setHasVedio(DictUtil.getIdByNameAndEnName("hasVedio", "否"));
			model.setHasOptions(DictUtil.getIdByNameAndEnName("hasOptions", "否"));
		}
		mnv.addObject(DEFAULT_PAGE_FORM, model);
		mnv.addObject("hasGroupImagesTypeList", ListTypeParameter.hasGroupImagesTypeList);
		mnv.addObject("hasContentTypeList", ListTypeParameter.hasContentTypeList);
		mnv.addObject("hasVedioTypeList", ListTypeParameter.hasVedioTypeList);
		mnv.addObject("hasOptionsTypeList", ListTypeParameter.hasOptionsTypeList);
		mnv.setViewName("view/admin/model/form");
		return mnv;
	 }

	@RequestMapping("addOrUpdate")
	@ResponseBody
	public ActionResult add(Model model,HttpServletRequest request) throws Exception{
		ActionResult result=new ActionResult();
		try {
			model.setCreateTime(DateUtil.dateToStr(new Date(), 12));
			modelService.saveOrUpdate(model);
			result.setSuccess(true);
			result.setMessage(RESULE_SUCCESS);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR);
		}
		return result;
	}
}

package com.youngcms.controller.admin;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.youngcms.bean.Advert;
import com.youngcms.controller.base.AdminBaseController;
import com.youngcms.service.AdvertPositionService;
import com.youngcms.service.AdvertService;
import com.youngcms.utils.DateUtil;
import com.youngcms.vo.ActionResult;
import com.youngcms.vo.QueryResult;
@Controller
@RequestMapping("/admin/advert/")
@SessionAttributes("pageForm")
public class AdvertController extends AdminBaseController{

     @Autowired
     private AdvertService advertService;
     @Autowired
     private AdvertPositionService advertPositionService;

     @RequestMapping("list")
     public ModelAndView list(Advert advert,HttpServletRequest request) throws Exception{
        QueryResult<Advert> queryResult=advertService.list(advert);
        advert.setPageDate(queryResult.getQueryResult());
        advert.setTotalCount(queryResult.getCount());
        ModelAndView mnv = new ModelAndView();
        mnv.addObject(DEFAULT_PAGE_VIEW,advert);
        mnv.setViewName("view/admin/advert/list");
        return mnv;
     }

     @RequestMapping("delete")
     @ResponseBody
     public ActionResult delete(Advert advert,HttpServletRequest request) throws Exception{
        ActionResult result=new ActionResult();
       try {
	     advertService.deleteByPrimaryKey(advert.getId());
	     result.setSuccess(true);
	     result.setMessage(RESULE_SUCCESS_DELETE);
		 } catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR_DELETE);
		}
		return result;
	 }

	 @RequestMapping("showForm")
	 public ModelAndView addForm(Advert advert,HttpServletRequest request) throws Exception{
		ModelAndView mnv = new ModelAndView();
		if(advert.getId()!=null){
			advert=advertService.selectByPrimaryKey(advert.getId());
		}
		mnv.addObject(DEFAULT_PAGE_FORM, advert);
		mnv.addObject("ADVERT_POSITION_LIST", advertPositionService.list().getQueryResult());
		mnv.setViewName("view/admin/advert/form");
		return mnv;
	 }

	@RequestMapping("addOrUpdate")
	@ResponseBody
	public ActionResult add(Advert advert,HttpServletRequest request) throws Exception{
		ActionResult result=new ActionResult();
		try {
			advert.setClickNum(0);
			advert.setCreateTime(DateUtil.getTodayTimeString());
			advertService.saveOrUpdate(advert);
			result.setSuccess(true);
			result.setMessage(RESULE_SUCCESS);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR);
		}
		return result;
	}
}

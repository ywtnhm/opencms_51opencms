package com.youngcms.controller.admin;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.youngcms.bean.FriendLink;
import com.youngcms.controller.base.AdminBaseController;
import com.youngcms.service.FriendLinkService;
import com.youngcms.utils.DateUtil;
import com.youngcms.utils.ListTypeParameter;
import com.youngcms.vo.ActionResult;
import com.youngcms.vo.QueryResult;
@Controller
@RequestMapping("/admin/friendLink/")
@SessionAttributes("pageForm")
public class FriendLinkController extends AdminBaseController{

     @Autowired
     private FriendLinkService friendLinkService;

     @RequestMapping("list")
     public ModelAndView list(FriendLink friendLink,HttpServletRequest request) throws Exception{
        QueryResult<FriendLink> queryResult=friendLinkService.list(friendLink);
        friendLink.setPageDate(queryResult.getQueryResult());
        friendLink.setTotalCount(queryResult.getCount());
        ModelAndView mnv = new ModelAndView();
        mnv.addObject(DEFAULT_PAGE_VIEW,friendLink);
        mnv.setViewName("view/admin/friendLink/list");
        return mnv;
     }

     @RequestMapping("delete")
     @ResponseBody
     public ActionResult delete(FriendLink friendLink,HttpServletRequest request) throws Exception{
        ActionResult result=new ActionResult();
       try {
	     friendLinkService.deleteByPrimaryKey(friendLink.getId());
	     result.setSuccess(true);
	     result.setMessage(RESULE_SUCCESS_DELETE);
		 } catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR_DELETE);
		}
		return result;
	 }

	 @RequestMapping("showForm")
	 public ModelAndView addForm(FriendLink friendLink,HttpServletRequest request) throws Exception{
		ModelAndView mnv = new ModelAndView();
		if(friendLink.getId()!=null){
			friendLink=friendLinkService.selectByPrimaryKey(friendLink.getId());
		}else{
			//friendLink.setClickNum(0);
			friendLink.setCreateTime(DateUtil.getTodayTimeString());
		}
	    mnv.addObject("FRIEND_LINK_TYPE_LIST",ListTypeParameter.friendLinkTypeList);
		mnv.addObject(DEFAULT_PAGE_FORM, friendLink);
		mnv.setViewName("view/admin/friendLink/form");
		return mnv;
	 }

	@RequestMapping("addOrUpdate")
	@ResponseBody
	public ActionResult add(FriendLink friendLink,HttpServletRequest request) throws Exception{
		ActionResult result=new ActionResult();
		try {
			friendLinkService.saveOrUpdate(friendLink);
			result.setSuccess(true);
			result.setMessage(RESULE_SUCCESS);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR);
		}
		return result;
	}
}

package com.youngcms.controller.admin;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.youngcms.bean.ReptileRule;
import com.youngcms.controller.base.AdminBaseController;
import com.youngcms.service.ContentReptileService;
import com.youngcms.service.ReptileRuleService;
import com.youngcms.utils.MessageDate;
import com.youngcms.utils.redis.RedisShardClient;
import com.youngcms.vo.ActionResult;
import com.youngcms.vo.QueryResult;
@Controller
@RequestMapping("/admin/reptileRule/")
@SessionAttributes("pageForm")
public class ReptileRuleController extends AdminBaseController{

     @Autowired
     private ReptileRuleService reptileRuleService;
     @Autowired
     private ContentReptileService contentReptileService;
     @Autowired
     private RedisShardClient redisShardClient;

     @RequestMapping("list")
     public ModelAndView list(ReptileRule reptileRule,HttpServletRequest request) throws Exception{
        QueryResult<ReptileRule> queryResult=reptileRuleService.list(reptileRule);
        reptileRule.setPageDate(queryResult.getQueryResult());
        reptileRule.setTotalCount(queryResult.getCount());
        ModelAndView mnv = new ModelAndView();
        mnv.addObject(DEFAULT_PAGE_VIEW,reptileRule);
        mnv.setViewName("view/admin/reptileRule/list");
        return mnv;
     }

     @RequestMapping("delete")
     @ResponseBody
     public ActionResult delete(ReptileRule reptileRule,HttpServletRequest request) throws Exception{
        ActionResult result=new ActionResult();
       try {
    	  reptileRuleService.deleteByPrimaryKey(reptileRule.getId());
	     result.setSuccess(true);
	     result.setMessage(RESULE_SUCCESS_DELETE);
		 } catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR_DELETE);
		}
		return result;
	 }

	 @RequestMapping("showForm")
	 public ModelAndView addForm(ReptileRule reptileRule,HttpServletRequest request) throws Exception{
		ModelAndView mnv = new ModelAndView();
		if(reptileRule.getId()!=null){
			reptileRule=reptileRuleService.selectByPrimaryKey(reptileRule.getId());
		}else{
			reptileRule.setCoding("UTF-8");
			reptileRule.setTimeFormat("yyyy-MM-dd HH:mm:ss");
			reptileRule.setNumber(500);
		}
		mnv.addObject(DEFAULT_PAGE_FORM, reptileRule);
		mnv.setViewName("view/admin/reptileRule/form");
		return mnv;
	 }

	@RequestMapping("addOrUpdate")
	@ResponseBody
	public ActionResult add(ReptileRule reptileRule,HttpServletRequest request) throws Exception{
		ActionResult result=new ActionResult();
		try {
			reptileRuleService.saveOrUpdate(reptileRule);
			result.setSuccess(true);
			result.setMessage(RESULE_SUCCESS);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR);
		}
		return result;
	}
	
	@RequestMapping("reptile")
	@ResponseBody
	public ActionResult reptile(ReptileRule reptileRule,HttpServletRequest request) throws Exception{
		ActionResult result=new ActionResult();
		try {
			
			MessageDate messageDate=new MessageDate();
			messageDate.setCommandName("reptileContentCommand");
			Map<String, Object> map=new HashMap<String, Object>();
			map.put("reptileRuleId", reptileRule.getId());
			messageDate.setParams(map);
			JSONObject jsonObject=new JSONObject(messageDate);
			redisShardClient.lpush(MessageDate.REDIS_KEY,jsonObject.toString());
			result.setSuccess(true);
			result.setMessage(RESULE_SUCCESS);
		} catch (Exception e) {
			System.out.println(e.toString());
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR);
		}
		return result;
	}
}

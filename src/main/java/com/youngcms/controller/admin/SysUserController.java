package com.youngcms.controller.admin;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.youngcms.bean.Role;
import com.youngcms.bean.SysUser;
import com.youngcms.bean.SysUserRole;
import com.youngcms.controller.base.AdminBaseController;
import com.youngcms.service.RoleService;
import com.youngcms.service.SysUserRoleService;
import com.youngcms.service.SysUserService;
import com.youngcms.utils.MD5;
import com.youngcms.vo.ActionResult;
import com.youngcms.vo.QueryResult;
@Controller
@RequestMapping("/admin/sysUser/")
@SessionAttributes("pageForm")
public class SysUserController extends AdminBaseController{

     @Autowired
     private SysUserService sysUserService;
     @Autowired
     private SysUserRoleService  sysUserRoleService;
     @Autowired
     private RoleService roleService;

     @RequestMapping("list")
     public ModelAndView list(SysUser sysUser,HttpServletRequest request) throws Exception{
        QueryResult<SysUser> queryResult=sysUserService.list(sysUser);
        sysUser.setPageDate(queryResult.getQueryResult());
        sysUser.setTotalCount(queryResult.getCount());
        ModelAndView mnv = new ModelAndView();
        mnv.addObject(DEFAULT_PAGE_VIEW,sysUser);
        mnv.setViewName("view/admin/sysUser/list");
        return mnv;
     }

     @RequestMapping("delete")
     @ResponseBody
     public ActionResult delete(SysUser sysUser,HttpServletRequest request) throws Exception{
        ActionResult result=new ActionResult();
       try {
	     sysUserService.deleteByPrimaryKey(sysUser.getId());
	     result.setSuccess(true);
	     result.setMessage(RESULE_SUCCESS_DELETE);
		 } catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR_DELETE);
		}
		return result;
	 }

	 @RequestMapping("showForm")
	 public ModelAndView addForm(SysUser sysUser,HttpServletRequest request) throws Exception{
		ModelAndView mnv = new ModelAndView();
		QueryResult<Role> queryResult=roleService.list();
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("sysUserId", sysUser.getId());
		QueryResult<SysUserRole> queryResult_1=sysUserRoleService.list(params);
		if(sysUser.getId()!=null){
			sysUser=sysUserService.selectByPrimaryKey(sysUser.getId());
		}
		mnv.addObject(DEFAULT_PAGE_FORM, sysUser);
		mnv.addObject("roleList", queryResult.getQueryResult());
		mnv.addObject("userRoleList", queryResult_1.getQueryResult());
		mnv.setViewName("view/admin/sysUser/form");
		return mnv;
	 }

	@RequestMapping("addOrUpdate")
	@ResponseBody
	public ActionResult add(SysUser sysUser,HttpServletRequest request) throws Exception{
		ActionResult result=new ActionResult();
		try {
			String roleIds=request.getParameter("roleIds");
			if(StringUtils.isNotBlank(sysUser.getPassword())){
				MD5 md5=new MD5();
				sysUser.setPassword(md5.getMD5ofStr(sysUser.getPassword()));
			}else{
				String old_password=request.getParameter("old_password");
				sysUser.setPassword(old_password);
			}
			sysUserService.addOrUpdate(sysUser, roleIds);
			result.setSuccess(true);
			result.setMessage(RESULE_SUCCESS);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR);
		}
		return result;
	}
}

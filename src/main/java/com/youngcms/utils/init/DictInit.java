package com.youngcms.utils.init;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.youngcms.bean.Dict;
import com.youngcms.service.DictService;
import com.youngcms.vo.DictBean;
import com.youngcms.vo.QueryResult;
/**
 * 加载字典表
 * @author fumiao
 * @date 2014-1-22
 */
public class DictInit implements ServletContextListener {
	/**
	 * 上下文对象
	 */
	private static WebApplicationContext wac;
	/**
	 * 	上下文销毁时执行方法
	 */
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	}
	/**
	 * 上下文初始时执行方法
	 */
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		wac = WebApplicationContextUtils.getWebApplicationContext(sce.getServletContext());
		DictService dictService = (DictService) wac.getBean("dictService");
		QueryResult<Dict> queryResult=dictService.list();
		DictBean.setDicts(queryResult.getQueryResult());
	}
	/**
	 * 获取上下文对象方法
	 * @return 上下方对象
	 */
	public static ApplicationContext getApplicationContext(){
		return wac;
	}

}

package com.youngcms.utils.command;

public abstract class BaseCommand{
	
	public abstract void execute(String params);
	
}

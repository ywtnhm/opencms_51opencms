package com.youngcms.utils;

import java.util.List;

import com.youngcms.vo.LabelValue;

public final class ListTypeParameter
{
	public final static List<LabelValue> isOpen = initIsOPen();
	
	public final static List<LabelValue> friendLinkTypeList = initFriendLinkTypeList();
	
	public final static List<LabelValue> advertPositionTypeList = initAdvertPositionTypeList();
	
	public final static List<LabelValue> moduleTypeList = initModuleTypeList();
	
	public final static List<LabelValue> isTopTypeList = initIsTopTypeList();
	
	public final static List<LabelValue> isCommentTypeList = initIsCommentTypeList();
	
	public final static List<LabelValue> hasGroupImagesTypeList = initHasGroupImagesTypeList();
	
	public final static List<LabelValue> hasVedioTypeList = initHasVedioTypeList();
	
	public final static List<LabelValue> hasContentTypeList = initHasContentTypeList();
	
	public final static List<LabelValue> hasOptionsTypeList = initHasOptionsTypeList();

	private static List<LabelValue> initIsOPen() {
		return DictUtil.getDictsByEnName("isOpen");
	}

	private static List<LabelValue> initHasOptionsTypeList() {
		return DictUtil.getDictsByEnName("hasOptions");
	}

	private static List<LabelValue> initHasContentTypeList() {
		return DictUtil.getDictsByEnName("hasContent");
	}

	private static List<LabelValue> initHasVedioTypeList() {
		return DictUtil.getDictsByEnName("hasVedio");
	}

	private static List<LabelValue> initHasGroupImagesTypeList() {
		return DictUtil.getDictsByEnName("hasGroupImages");
	}

	private static List<LabelValue> initIsCommentTypeList() {
		return DictUtil.getDictsByEnName("isComment");
	}

	private static List<LabelValue> initIsTopTypeList() {
		return DictUtil.getDictsByEnName("isTop");
	}

	private static List<LabelValue> initFriendLinkTypeList() {
		return DictUtil.getDictsByEnName("friendLinkType");
	}
	
	private static List<LabelValue> initModuleTypeList() {
		return DictUtil.getDictsByEnName("moduleType");
	}

	private static List<LabelValue> initAdvertPositionTypeList() {
		return DictUtil.getDictsByEnName("advertPositionType");
	}

}

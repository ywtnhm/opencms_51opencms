package com.youngcms.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youngcms.bean.FriendLink;
import com.youngcms.dao.FriendLinkMapper;
import com.youngcms.dao.base.MapperSupport;
import com.youngcms.service.base.impl.ServiceSupport;
@Service
public class FriendLinkService extends ServiceSupport<FriendLink> {
   @Autowired
   private FriendLinkMapper friendLinkMapper;

@Override
public MapperSupport<FriendLink> getMapperSupport() {
	// TODO Auto-generated method stub
	return friendLinkMapper;
}
}

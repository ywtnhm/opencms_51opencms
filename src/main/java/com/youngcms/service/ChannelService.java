package com.youngcms.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youngcms.bean.Channel;
import com.youngcms.bean.ChannelModel;
import com.youngcms.dao.ChannelMapper;
import com.youngcms.dao.ChannelModelMapper;
import com.youngcms.dao.base.MapperSupport;
import com.youngcms.service.base.impl.ServiceSupport;

@Service
public class ChannelService extends ServiceSupport<Channel> {
	@Autowired
	private ChannelMapper channelMapper;
	
	@Autowired
	private ChannelModelMapper channelModelMapper;

	@Override
	public MapperSupport<Channel> getMapperSupport() {
		return channelMapper;
	}

	public Channel selectByCode(String code) {
		return channelMapper.selectByCode(code);
	}
	
	public void saveOrUpdate(Channel channel,String modelIds){
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("channelId",channel.getId());
		channelModelMapper.deleteByParams(params);
		if (StringUtils.isNotBlank(modelIds)) {
			String[] ids=StringUtils.split(modelIds,",");
			for (int i = 0; i < ids.length; i++) {
				ChannelModel channelModel = new ChannelModel();
				channelModel.setChannelId(channel.getId());
				channelModel.setModelId(Integer.valueOf(ids[i]));
				channelModelMapper.insertSelective(channelModel);
			}
		}
		if(channel.getId()!=null){
			channelMapper.updateByPrimaryKeySelective(channel);
		}else{
			channelMapper.insertSelective(channel);
		}
	}
}

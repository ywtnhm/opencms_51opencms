package com.youngcms.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youngcms.bean.Flow;
import com.youngcms.dao.FlowMapper;
import com.youngcms.dao.base.MapperSupport;
import com.youngcms.service.base.impl.ServiceSupport;
@Service
public class FlowService extends ServiceSupport<Flow> {
   @Autowired
   private FlowMapper flowMapper;

@Override
public MapperSupport<Flow> getMapperSupport() {
	return flowMapper;
}
}

package com.youngcms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youngcms.bean.Model;
import com.youngcms.dao.ModelMapper;
import com.youngcms.dao.base.MapperSupport;
import com.youngcms.service.base.impl.ServiceSupport;
@Service
public class ModelService extends ServiceSupport<Model> {

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public MapperSupport<Model> getMapperSupport() {
		return modelMapper;
	}
	
	
}

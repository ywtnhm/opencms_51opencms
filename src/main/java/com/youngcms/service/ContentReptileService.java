package com.youngcms.service;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.youngcms.bean.Content;
import com.youngcms.bean.ContentReptile;
import com.youngcms.bean.ContentText;
import com.youngcms.dao.ContentMapper;
import com.youngcms.dao.ContentReptileMapper;
import com.youngcms.dao.ContentTextMapper;
import com.youngcms.dao.base.MapperSupport;
import com.youngcms.service.base.impl.ServiceSupport;
import com.youngcms.utils.DateUtil;
import com.youngcms.utils.DictUtil;
@Service
public class ContentReptileService extends ServiceSupport<ContentReptile> {

	@Autowired
	private ContentReptileMapper contentReptileMapper;
	
	@Autowired
	private ContentMapper contentMapper;
	
	@Autowired
	private ContentTextMapper contentTextMapper;

	@Override
	public MapperSupport<ContentReptile> getMapperSupport() {
		return contentReptileMapper;
	}
    
	@Transactional(propagation=Propagation.REQUIRED,rollbackFor=Exception.class)
	public void toContent(String ids, String channelId) throws Exception{
		String[] reptileIds=StringUtils.split(ids, ",");
		for(int i=0;i<reptileIds.length;i++){
			String reptileId=reptileIds[i];
			if(StringUtils.isNotBlank(reptileId)){
				ContentReptile contentReptile=contentReptileMapper.selectByPrimaryKey(Integer.valueOf(reptileId));
				Content content=new Content();
				content.setTitle(contentReptile.getTitle());
				content.setShortTitle(contentReptile.getTitle());
				content.setChannelId(Integer.valueOf(channelId));
				content.setClickNum(0);
				content.setCreateTime(DateUtil.dateToStr(new Date(), 12));
				content.setDescription(contentReptile.getDescription());
				content.setSource(contentReptile.getSource());
				content.setAuthor(contentReptile.getAuthor());
				content.setModelId(4);
				content.setTitleColor("#000000");
				content.setWeight(10);
				content.setIsTop(DictUtil.getIdByNameAndEnName("isTop","否"));
				content.setIsComment(DictUtil.getIdByNameAndEnName("isComment","是"));
				content.setTemplate("default/content/news_detail.html");
				contentMapper.insertSelective(content);
				ContentText contentText=new ContentText();
				contentText.setText(contentReptile.getContent());
				contentText.setContentId(content.getId());
				contentTextMapper.insertSelective(contentText);
				contentReptileMapper.deleteByPrimaryKey(Integer.valueOf(reptileId));
			}
		}
	}
	
	
}

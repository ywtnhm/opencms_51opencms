package com.youngcms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youngcms.bean.User;
import com.youngcms.dao.UserMapper;
import com.youngcms.dao.base.MapperSupport;
import com.youngcms.service.base.impl.ServiceSupport;
@Service
public class UserService extends ServiceSupport<User> {

	@Autowired
	private UserMapper userMapper;

	@Override
	public MapperSupport<User> getMapperSupport() {
		return userMapper;
	}
	
	
}

package com.youngcms.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youngcms.bean.SysUser;
import com.youngcms.bean.SysUserRole;
import com.youngcms.dao.SysUserMapper;
import com.youngcms.dao.SysUserRoleMapper;
import com.youngcms.dao.base.MapperSupport;
import com.youngcms.service.base.impl.ServiceSupport;

@Service
public class SysUserService extends ServiceSupport<SysUser> {
	@Autowired
	private SysUserMapper sysUserMapper;
	@Autowired
	private SysUserRoleMapper sysUserRoleMapper;

	@Override
	public MapperSupport<SysUser> getMapperSupport() {
		return sysUserMapper;
	}

	public void addOrUpdate(SysUser sysUser, String roleIds) {
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("sysUserId",sysUser.getId());
		sysUserRoleMapper.deleteByParams(params);
		if (StringUtils.isNotBlank(roleIds)) {
			String[] ids=StringUtils.split(roleIds,",");
			for (int i = 0; i < ids.length; i++) {
				SysUserRole sysUserRole = new SysUserRole();
				sysUserRole.setSysUserId(sysUser.getId());
				sysUserRole.setRoleId(Integer.valueOf(ids[i]));
				sysUserRoleMapper.insertSelective(sysUserRole);
			}
		}
		if(sysUser.getId()!=null){
			sysUserMapper.updateByPrimaryKeySelective(sysUser);
		}else{
			sysUserMapper.insertSelective(sysUser);
		}
	}

}

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
<script type="text/javascript">
$(function(){
	var roleIds="";
	var checkboxs=$("input[type='checkbox']:checked");
	for(var i=0;i<checkboxs.length;i++){
		roleIds+=checkboxs[i].value+",";
	}
	 if(roleIds!=""){
		 roleIds=roleIds.substring(0,roleIds.length-1);	
	}
	$("#roleIds").val(roleIds);
});
function chooseRole(wel){
	var roleIds="";
	var checkboxs=$("input[type='checkbox']:checked");
	for(var i=0;i<checkboxs.length;i++){
		roleIds+=checkboxs[i].value+",";
	}
	 if(roleIds!=""){
		 roleIds=roleIds.substring(0,roleIds.length-1);	
	    }
		$("#roleIds").val(roleIds);
}
</script>
</head>
<body>
    <form:form action="${path}/admin/sysUser/addOrUpdate" id="form_add_update" name="form_add_update" modelAttribute="pageForm">
             <form:hidden path="id" />
            <input type="hidden" id="roleIds" name="roleIds"/>
            <input type="hidden" id="old_password" name="old_password" value="${pageForm.password }"/>
	    	<table class="formtable">
		    <tr>
	               <td colspan="4"><div class="formtitle"><span>基本信息</span></div></td>
	        </tr>
			<tr>
				<td>登录名</td>
				<td>
				   <form:input path="loginName" cssClass="dfinput" datatype="required" errormsg="不能为空"/>
				</td>
				<td>真实姓名</td>
				<td><form:input path="realName" cssClass="dfinput" datatype="required" errormsg="不能为空"/></td>
			</tr>
			<tr>
				<td>密码</td>
				<td><input type="password" placeholder="不修改密码，请留空" name="password" id="password" class="dfinput" /></td>
				<td>重复密码</td>
				<td><input type="password" placeholder="不修改密码，请留空" id="rePassword" class="dfinput" /></td>
			</tr>
			<tr>
				<td>角色</td>
				<td colspan="3">
				 <c:forEach items="${roleList }" var="role">
				   <span>
				        <c:set var="YN" value="0"></c:set>
				        <c:forEach items="${userRoleList }" var="sysUserRole">
							<c:if test="${sysUserRole.roleId==role.id }">
							   <c:set var="YN" value="1"></c:set>
							</c:if>      
					    </c:forEach>
					    <c:choose>
							<c:when test="${YN==1 }">
								<input type="checkbox" checked="checked" value="${role.id }" onclick="chooseRole(this)" />${role.name }
							</c:when>
							<c:otherwise>
								<input type="checkbox" value="${role.id }" onclick="chooseRole(this)" />${role.name }
							</c:otherwise>
						</c:choose>
				   </span>
				 </c:forEach>
				</td>
			</tr>
			</table>
     </form:form>
</body>
</html>

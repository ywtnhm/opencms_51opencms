<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
</head>
<body>
    <form:form action="#" id="listForm" name="listForm">
    <jsp:include page="/view/admin/common/place.jsp" />
    <div class="rightinfo">
    <div class="tools">
    	<ul class="toolbar">
	        <li onclick="showForm('用户添加','${path}/admin/dict/showForm')"><span><i class="Hui-iconfont">&#xe600;</i></span>添加</li>
        </ul>
    </div>
    <table class="tablelist">
    	<thead>
	    	<tr>
	        <th width="5%"><input type="checkbox" checked="checked"/></th>
	        <th width="5%">编号</th>
	        <th width="10%">名称</th>
	        <th width="10%">类型中文名称</th>
	        <th width="10%">类型英文名称</th>
	        <th width="10%">排序</th>
         <th width="5%">操作</th>	        </tr>
       </thead>
        <tbody>
        <c:forEach items="${pageView.pageDate }" var="bean">
	        <tr>
		        <td><input name="id" type="checkbox" value="" /></td>
		        <td>${bean.id }</td>
		        <td>${bean.name }</td>
		        <td>${bean.chName }</td>
		        <td>${bean.enName }</td>
		        <td>${bean.sort }</td>
		        <td class="td-manage">
		        <a title="编辑" href="javascript:showForm('用户修改','${path}/admin/dict/showForm?id=${bean.id }')" class="ml-5" style="text-decoration: none">
		            <i class="Hui-iconfont">&#xe6df;</i>
		        </a>
		        <a title="删除" href="javascript:del('${path }/admin/dict/delete?id=${bean.id}')" class="ml-5" style="text-decoration: none">
		            <i class="Hui-iconfont">&#xe6e2;</i>
		        </a>
				</td>
			</tr>
       </c:forEach>
        </tbody>
    </table>
   <jsp:include page="/view/admin/common/pageinfo.jsp" />
    </div>
    </form:form>
</body>
</html>

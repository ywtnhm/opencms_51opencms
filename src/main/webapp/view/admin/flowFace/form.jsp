<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
<script type="text/javascript">
$(document).ready(function(e) {
	$(".select2").uedSelect({
		width : 200  
	});
});
function chooseRole(wel){
	var roleIds="";
	var roleNames="";
	var checkboxs=$("input[type='checkbox']:checked");
	for(var i=0;i<checkboxs.length;i++){
		roleIds+="("+checkboxs[i].value+"),";
		roleNames+=checkboxs[i].nextSibling.nodeValue.replace(/(^\s+)|(\s+$)/g,"")+",";
	}
	 if(roleIds!=""){
		 roleIds=roleIds.substring(0,roleIds.length-1);	
	 }
	 if(roleNames!=""){
		 roleNames=roleNames.substring(0,roleNames.length-1);	
	 }
		$("#roleIds").val(roleIds);
		$("#roleNames").val(roleNames);
}
</script>
</head>
<body>
    <form:form action="${path}/admin/flowFace/addOrUpdate" id="form_add_update" name="form_add_update" modelAttribute="pageForm">
       <form:hidden path="id" />
      <form:hidden path="roleIds" />
      <form:hidden path="roleNames" />
      <table class='formtable'>
        <tr>
          <td colspan="4"><div class="formtitle"><span>基本信息</span></div></td>
        </tr>
        <tr>
          <td>节点名称</td>
          <td><form:input path="name" cssClass="dfinput"/></td>
          <td>工作流组</td>
          <td>
            <form:select path="flowId" cssClass="select2">
               <form:options items="${FLOW_LIST }"  itemValue="id" itemLabel="name"/>
            </form:select>
         </td>
       </tr>
       <tr>
          <td>介绍</td>
          <td><form:input path="description" cssClass="dfinput"/></td>
       </tr>
       <tr>
				<td>角色</td>
				<td colspan="3">
				 <c:forEach items="${ROLE_LIST }" var="role">
				   <span>
				      <c:set var="R" value="(${role.id })"></c:set>
				      <c:choose>
				         <c:when test="${fn:contains(pageForm.roleIds,R)}">
				             <input type="checkbox" checked="checked" value="${role.id }" onclick="chooseRole(this)" />${role.name }
				         </c:when>
				         <c:otherwise>
				             <input type="checkbox" value="${role.id }" onclick="chooseRole(this)" />${role.name }
				         </c:otherwise>
				      </c:choose>
					  
				   </span>
				 </c:forEach>
				</td>
	   </tr>
	   </table>
     </form:form>
</body>
</html>

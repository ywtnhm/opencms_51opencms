<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript">
var setting = {
	async : {
		enable : true,
		url : path + "/admin/channel/simpleNodes",
		autoParam : [ "id=parent_channel_id" ]
	},
	data: {
		simpleData: {
			enable: true
		}
	},
	check : {
		enable : true,
		chkStyle: "radio",
		radioType: "all"
	},
	callback : {
		onClick : onClick,
		onCheck : onCheck
	}
};
function onClick(e, treeId, treeNode) {
	var zTree = $.fn.zTree.getZTreeObj("treeDemo");
	zTree.checkNode(treeNode, !treeNode.checked, null, true);
	return false;
}
function onCheck(e, treeId, treeNode) {
	var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
	nodes = zTree.getCheckedNodes(true),
	v = "";
	id = "";
	for (var i=0, l=nodes.length; i<l; i++) {
		v  = nodes[i].name ;
		id = nodes[i].id ;
	}
	$("#pName").attr("value", v);
	$("#pId").attr("value", id);
}

function showMenu() {
	var cityObj = $("#pName");
	var cityOffset = $("#pName").offset();
	$("#menuContent").css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight()-10 + "px"}).slideDown("fast");

	$("body").bind("mousedown", onBodyDown);
}
function hideMenu() {
	$("#menuContent").fadeOut("fast");
	$("body").unbind("mousedown", onBodyDown);
}
function onBodyDown(event) {
	if (!(event.target.id == "menuBtn" || event.target.id == "pName" || event.target.id == "menuContent" || $(event.target).parents("#menuContent").length>0)) {
		hideMenu();
	}
}
$(document).ready(function(){
	$.fn.zTree.init($("#treeDemo"), setting);
});
</script>
<div id="menuContent">
	    <ul id="treeDemo" class="ztree ztreeSty"></ul>
 </div>
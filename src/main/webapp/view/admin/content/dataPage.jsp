<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
<script type="text/javascript">
var setting = {
		async: {
			enable: true,
			url:path+"/admin/channel/simpleNodes",
			autoParam:["id=parent_channel_id"]
		},
		data: {
			simpleData: {
				enable: true
			}
		},
		callback: {
			onClick: onClick
		}
	};
$(document).ready(function(){
	$.fn.zTree.init($("#treeDemo"), setting);
	createItabs(1);
});
function onClick(event, treeId, treeNode, clickFlag) {
	createItabs(treeNode.id);
}
function createItabs(channelId){
	$.ajax({  
        type : "POST",
        url : path+"/admin/content/getChannelModel",
        data : {channelId:channelId},
        dataType: 'json',
        success : function(result) {
        	    var channelModel="";
        	    var modelDiv="";
        		$.each(result, function(i, item){      
        			channelModel+="<li><a href='#ifreamDiv"+i+"'>"+item.modelName+"</a></li>"; 
        			modelDiv+="<div class='tabson' id='ifreamDiv"+i+"'  style='float:left;width: 100%;margin: 0;padding: 0;'>"+
        			          "<iframe class='iframepage' src='"+path+"/admin/content/list?modelId="+item.modelId+"&channelId="+channelId+"' width='100%' height='90%'></iframe>"+
        	                  "</div>";
        		}); 
        		channelModel+="<li><a href='#ifreamDiv99'>待审核</a></li>";
        		modelDiv+="<div class='tabson' id='ifreamDiv99'  style='float:left;width: 100%;margin: 0;padding: 0;'>"+
		                  "<iframe class='iframepage' src='"+path+"/admin/content/flowList?channelId="+channelId+"' width='100%' height='90%'></iframe>"+
                          "</div>";
        		$("#channelModel").html(channelModel);
        		$("#modelDiv").html(modelDiv);
        		$(".itab ul").idTabs();
        }  
    });  
}
</script>
</head>
<body>
    <form:form action="#" id="listForm" name="listForm">
    <jsp:include page="/view/admin/common/place.jsp" />
    <div class="rightinfo">
    <div style="float: left;width: 10%;height: 90%;border-right: 1px solid #cbcbcb;">
		<ul id="treeDemo" class="ztree"></ul> 
	</div>
	
	<div id="tabBox" class="tabBox" style="float: left;width: 89%;height: 94%;margin-left: 5px;"> 
	   <div class="itab">
		  <ul id="channelModel"> 
		     <%-- <c:forEach items="${channelModelList}" var="channelModel" varStatus="i">
		        <li><a href="#ifreamDiv${i.index }">${channelModel.modelName }</a></li>
		     </c:forEach>
		     <li><a href="#ifreamDiv99">待审核</a></li> --%>
		  </ul>
	   </div> 
	   <div id="modelDiv">
	   
	   </div>
	   
	</div>
    </div>
    </form:form>
</body>
</html>

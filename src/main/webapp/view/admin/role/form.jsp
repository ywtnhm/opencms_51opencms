<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
<script type="text/javascript">
var roleId='${pageForm.id}';
var setting = {
		async : {
			enable : true,
			url : path + "/admin/module/simpleNodes",
			autoParam : [ "id=parent_channel_id" ],
			otherParam: ["roleId", roleId]
		},
		data: {
			simpleData: {
				enable: true
			}
		},
		check : {
			enable : true,
		},
		callback:{
            onCheck:onCheck
        }
	};
$(document).ready(function(){
	$.fn.zTree.init($("#treeDemo"), setting);
});
function hideMenu() {
	$("#menuContent").fadeOut("fast");
	$("body").unbind("mousedown", onBodyDown);
}
function onBodyDown(event) {
	if (!(event.target.id == "menuBtn" || event.target.id == "channelName" || event.target.id == "menuContent" || $(event.target).parents("#menuContent").length>0)) {
		hideMenu();
	}
}
function showMenu() {
	var cityObj = $("#chooseModule");
	var cityOffset = $("#chooseModule").offset();
	$("#menuContent").css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight()-10 + "px"}).slideDown("fast");

	$("body").bind("mousedown", onBodyDown);
}
function onCheck(e,treeId,treeNode){
    var treeObj=$.fn.zTree.getZTreeObj("treeDemo");
    var nodes=treeObj.getCheckedNodes(true);
    var moduleIds="";
    var moduleNames="";
    for(var i=0;i<nodes.length;i++){
      moduleIds+=nodes[i].id + ",";
      moduleNames+=nodes[i].name + "_";
    }
    $("#moduleIds").val(moduleIds.substring(0, moduleIds.length-1));
    //$("#moduleNames").html(moduleNames.substring(0, moduleNames.length-1));
}
</script>
</head>
<body>
    <form:form action="${path}/admin/role/addOrUpdate" id="form_add_update" name="form_add_update" modelAttribute="pageForm">
        <form:hidden path="id" />
        <form:hidden path="moduleIds"/>
	    <table class="formtable">
					<tr>
					    <td>角色名称</td>
						<td><form:input path="name" cssClass="dfinput"/></td>
					</tr>
					<tr>
						<td>权限分配</td>
					    <td>
					       <a href="javascript:showMenu();" id="chooseModule" name="chooseModule" class="add">权限分配</a>
					    </td>
					</tr>
					<%-- <tr>
						<td>所有权限</td>
					    <td>
					       <form:textarea path="moduleNames"  cssClass="textinput" />
					    </td>
					</tr> --%>
		</table>
     </form:form>
      <div id="menuContent">
	    <ul id="treeDemo" class="ztree ztreeSty"></ul>
     </div>
</body>
</html>

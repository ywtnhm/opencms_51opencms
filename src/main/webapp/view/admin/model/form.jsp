<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
</head>
<body>
    <form:form action="${path}/admin/model/addOrUpdate" id="form_add_update" name="form_add_update" modelAttribute="pageForm">
       <form:hidden path="id" />
      <table class='formtable'>
        <tr>
          <td colspan="4"><div class="formtitle"><span>基本信息</span></div></td>
        </tr>
       <tr>
          <td>模型名称</td>
          <td><form:input path="name" cssClass="dfinput"/></td>
          <td>模板前缀</td>
          <td><form:input path="tempPrefix" cssClass="dfinput"/></td>
       </tr>
       <tr>
          <td>是否有内容</td>
          <td><form:radiobuttons path="hasContent"  items="${hasContentTypeList}" itemLabel="label" itemValue="value" /></td>
          <td>是否有视频</td>
          <td><form:radiobuttons path="hasVedio" items="${hasVedioTypeList}" itemLabel="label" itemValue="value" /></td>
       </tr>
       <tr>
          <td>是否有组图</td>
          <td><form:radiobuttons path="hasGroupImages" items="${hasGroupImagesTypeList}" itemLabel="label" itemValue="value" /></td>
          <td>是否有附件</td>
          <td><form:radiobuttons path="hasOptions"  items="${hasOptionsTypeList}" itemLabel="label" itemValue="value" /></td>
       </tr>
       </table>
     </form:form>
</body>
</html>

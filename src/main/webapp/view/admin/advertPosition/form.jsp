<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
<script type="text/javascript">
$(document).ready(function(e) {
	$(".select2").uedSelect({
		width : 200  
	});
});
</script>
</head>
<body>
    <form:form action="${path}/admin/advertPosition/addOrUpdate" id="form_add_update" name="form_add_update" modelAttribute="pageForm">
       <form:hidden path="id" />
      <table class='formtable'>
        <tr>
          <td colspan="4"><div class="formtitle"><span>基本信息</span></div></td>
        </tr>
       <tr>
          <td>名称</td>
          <td><form:input path="name" cssClass="dfinput" datatype="required"  errormsg="不能为空"/></td>
          <td>类型</td>
          <td>
             <form:select path="type" cssClass="select2">
                  <form:options items="${ADVERT_POSITION_TYPE_LIST }" itemValue="value" itemLabel="label" datatype="required"  errormsg="不能为空" />
             </form:select>
          </td>
       </tr>
       <tr>
          <td>宽度</td>
          <td><form:input path="width" cssClass="dfinput" datatype="required"  errormsg="不能为空"/></td>
          <td>高度</td>
          <td><form:input path="height" cssClass="dfinput" datatype="required"  errormsg="不能为空"/></td>
       </tr>
       <tr>
          <td>是否开启</td>
          <td>
             <form:radiobuttons path="isOpen" items="${IS_OPEN }" itemValue="value" itemLabel="label" />
          </td>
       </tr>
       </table>
     </form:form>
</body>
</html>
